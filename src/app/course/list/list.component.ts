import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { CourseDataSource } from './list-datasource';
import { CourseService } from 'src/app/service/course.service';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-course-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class CourseListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CourseDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'content', 'lecturer'];
  constructor(private courseService: CourseService) { }
  ngOnInit() {
    this.courseService.getCourses().subscribe(
      courses => {
        this.dataSource = new CourseDataSource(this.paginator, this.sort);
        this.dataSource.data = courses;
      });

  }
}
